package me.sarkhad.tasktracker.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author a.dobrynin
 * @since 0.0.3
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository repository;

    @Test
    public void searchUsersByPartOfNameShouldIgnoreFirstNameCase() {
        User user = new User();
        user.setFirstName("Ramsay");
        user.setLastName("Bolton");
        user.setEmail("r.bolton@example.com");
        user.setPassword("encodedPassword");

        entityManager.persist(user);

        Page<User> page = repository.searchUsersByPartOfName("ams", mock(Pageable.class));

        assertEquals(1, page.getTotalElements());
        assertTrue(page.getContent().contains(user));
    }

    @Test
    public void searchUsersByPartOfNameShouldIgnoreLastNameCase() {
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Smith");
        user.setEmail("j.smith@example.com");
        user.setPassword("encodedPassword");

        entityManager.persist(user);

        Page<User> page = repository.searchUsersByPartOfName("smi", mock(Pageable.class));

        assertEquals(1, page.getTotalElements());
        assertTrue(page.getContent().contains(user));
    }

    @Test
    public void testFindOneByEmail() {
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Smith");
        user.setEmail("j.smith@example.com");
        user.setPassword("encodedPassword");

        entityManager.persist(user);

        User foundUser = repository.findOneByEmail("j.smith@example.com");

        assertEquals(user.getFirstName(), foundUser.getFirstName());
        assertEquals(user.getLastName(), foundUser.getLastName());
        assertEquals(user.getEmail(), foundUser.getEmail());
    }

}
