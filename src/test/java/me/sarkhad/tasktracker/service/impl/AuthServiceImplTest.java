package me.sarkhad.tasktracker.service.impl;

import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.domain.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author a.dobrynin
 * @since 0.0.2
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AuthServiceImpl service;

    @Test
    public void isValid_False_IfNoUserWithGivenEmail() {
        String email = "j.smith@emails.com";

        doReturn(null).when(userRepository).findOneByEmail(email);

        Map<String, Object> validationResult = service.isValidEmail(email);

        assertFalse("Validation result should be false", (boolean) validationResult.get("isValid"));
        assertEquals("Should be validation result message", "Email does not exists in the system", validationResult.get("message"));
    }

    @Test
    public void isValid_True_IfFoundUserWithGivenEmail() {
        String email = "j.smith@emails.com";

        doReturn(mock(User.class)).when(userRepository).findOneByEmail(email);

        Map<String, Object> validationResult = service.isValidEmail(email);

        assertTrue("Validation result should be true", (boolean) validationResult.get("isValid"));
        assertFalse("Shouldn't be validation result message", validationResult.containsKey("message"));
    }

    @Test
    public void authenticate_Empty_IfNoUserWithGivenEmail() {
        String username = "v.pupkin@gmail.com";

        doReturn(null).when(userRepository).findOneByEmail(username);

        assertEquals("", "", service.authenticate(username, null));
    }

}
