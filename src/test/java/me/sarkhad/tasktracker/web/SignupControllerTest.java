package me.sarkhad.tasktracker.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.sarkhad.tasktracker.service.EmailVerificationService;
import me.sarkhad.tasktracker.service.SignupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author a.dobrynin
 * @since 0.0.3
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = SignupController.class, secure = false)
public class SignupControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SignupService service;

    @MockBean
    private EmailVerificationService emailVerificationService;

    @Test
    public void testConfirm() throws Exception {
        String token = "ai5zbm93QGV4YW1wbGUuY29t";

        doNothing().when(emailVerificationService).verificate(token);

        mvc.perform(post("/signup/confirm").content(token))
                .andExpect(status().isOk());

        verify(emailVerificationService).verificate(token);
    }

    @Test
    public void testCreateUserShouldBeFoundStatusIfUserIsAlreadyRegistered() throws Exception {
        Map<Object, Object> userDto = new HashMap<>();
        userDto.put("firstName", "Jon");
        userDto.put("lastName", "Snow");
        userDto.put("email", "j.snow@example.com");
        userDto.put("password", "rawPassword");
        userDto.put("isManager", true);

        doReturn(false).when(service).register(userDto);

        mvc.perform(post("/signup").content(new ObjectMapper().writeValueAsString(userDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());

        verify(service).register(userDto);
    }

    @Test
    public void testCreateUserShouldBeCreatedStatusIfUserIsRegistered() throws Exception {
        Map<Object, Object> userDto = new HashMap<>();
        userDto.put("firstName", "Sansa");
        userDto.put("lastName", "Stark");
        userDto.put("email", "s.stark@example.com");
        userDto.put("password", "rawPassword");
        userDto.put("isManager", false);

        doReturn(true).when(service).register(userDto);

        mvc.perform(post("/signup").content(new ObjectMapper().writeValueAsString(userDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(service).register(userDto);
    }

}
