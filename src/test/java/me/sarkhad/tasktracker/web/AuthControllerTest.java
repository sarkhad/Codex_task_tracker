package me.sarkhad.tasktracker.web;

import me.sarkhad.tasktracker.domain.Role;
import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.service.AuthService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author a.dobrynin
 * @since 0.0.3
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = AuthController.class, secure = false)
public class AuthControllerTest {

    @MockBean
    private AuthService authService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void testUser() throws Exception {
        User user = new User();
        user.setRoles(Collections.singletonList(Role.DEVELOPER));
        user.setFirstName("Arya");
        user.setLastName("Stark");
        user.setEmail("a.stark@email.com");

        doReturn(user).when(authService).getCurrentUser();

        mvc.perform(get("/current"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json("{\"id\":null,\"firstName\":\"Arya\",\"lastName\":\"Stark\",\"email\":\"a.stark@email.com\",\"roles\":[\"DEVELOPER\"],\"username\":\"a.stark@email.com\"}"));

        verify(authService).getCurrentUser();
    }

}
