package me.sarkhad.tasktracker.service.impl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.domain.UserRepository;
import me.sarkhad.tasktracker.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author a.dobrynin
 * @since 0.0.2
 */
@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();

        return userRepository.findOneByEmail(loggedUsername);
    }

    /**
     * Check if there is already registered user with given email
     *
     * @param email email to check
     * @return validation result dto
     */
    @Override
    public Map<String, Object> isValidEmail(String email) {
        Map<String, Object> validationResult = new HashMap<>();

        if (userRepository.findOneByEmail(email) != null) {
            validationResult.put("isValid", true);
        } else {
            validationResult.put("isValid", false);
            validationResult.put("message", "Email does not exists in the system");
        }

        return validationResult;
    }

    @Override
    public String authenticate(String username, String password) {
        String token;
        User appUser = userRepository.findOneByEmail(username);

        if (appUser != null) {
            //TODO add token expiration
            if (appUser.isEnabled() && passwordEncoder.matches(password, appUser.getPassword())) {
                token = Jwts.builder().setSubject(username).claim("roles", appUser.getRoles()).setIssuedAt(new Date())
                        .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
                return token;
            } else {
                return "disabled";
            }
        } else {
            return "";
        }
    }

}
