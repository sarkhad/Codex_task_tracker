package me.sarkhad.tasktracker.service.impl;

import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.domain.UserRepository;
import me.sarkhad.tasktracker.service.EmailVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import javax.transaction.Transactional;

/**
 * Service for confirmation user emails
 *
 * @author a.dobrynin
 * @since 0.0.2
 */
@Service
public class EmailVerificationServiceImpl implements EmailVerificationService {

    private final MailSender sender;

    private final UserRepository userRepository;

    private final Environment environment;

    @Autowired
    public EmailVerificationServiceImpl(MailSender sender, UserRepository userRepository, Environment environment) {
        this.sender = sender;
        this.userRepository = userRepository;
        this.environment = environment;
    }

    @Override
    public void sendVerificationLink(String email) {
        //TODO implement temporal token confirmation
        //Yes, I know that using of Base64 is not secure but it's simple solution and pretty suitable for non production project.
        String token = Base64Utils.encodeToUrlSafeString(email.getBytes());

        SimpleMailMessage message = new SimpleMailMessage();

        //TODO hardcoded properties
        message.setTo(email);
        message.setSubject("CodeX Task Tracker email verification");
        message.setText("Thanks for registration. To confirm email and activate your account click this link: " + environment.getProperty("environment.frontend.confirm-email-link") + token);

        sender.send(message);
    }

    @Override
    @Transactional
    public void verificate(String token) {
        String email = new String(Base64Utils.decodeFromUrlSafeString(token));

        User user;
        if ((user = userRepository.findOneByEmail(email)) != null) {
            user.setVerified(true);
            userRepository.saveAndFlush(user);
        }
    }

}
