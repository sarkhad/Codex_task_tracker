package me.sarkhad.tasktracker.service.impl;

import me.sarkhad.tasktracker.domain.Role;
import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.domain.UserRepository;
import me.sarkhad.tasktracker.service.EmailVerificationService;
import me.sarkhad.tasktracker.service.SignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author a.dobrynin
 * @since 0.0.2
 */
@Service
public class SignupServiceImpl implements SignupService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final EmailVerificationService emailVerificationService;

    @Autowired
    public SignupServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailVerificationService emailVerificationService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailVerificationService = emailVerificationService;
    }

    /**
     * @param userDTO user dto
     * @return if user is registered
     */
    @Override
    @Transactional
    public boolean register(Map<Object, Object> userDTO) {
        if (userRepository.findOneByEmail(userDTO.get("email").toString()) != null) {
            return false;
        }

        User user = new User();
        user.setEmail(userDTO.get("email").toString());
        user.setFirstName(userDTO.get("firstName").toString());
        user.setLastName(userDTO.get("lastName").toString());
        user.setPassword(passwordEncoder.encode(userDTO.get("password").toString()));

        List<Role> roles = new ArrayList<>();
        roles.add(getRole(userDTO.get("isManager").toString()));
        user.setRoles(roles);

        emailVerificationService.sendVerificationLink(user.getEmail());

        userRepository.saveAndFlush(user);

        return true;
    }

    private Role getRole(String isManager) {
        return Boolean.parseBoolean(isManager) ? Role.MANAGER : Role.DEVELOPER;
    }

    /**
     * Check if there is already registered user with given email
     *
     * @param email email to check
     * @return validation result dto
     */
    @Override
    public Map<String, Object> isValidEmail(String email) {
        Map<String, Object> validationResult = new HashMap<>();

        if (userRepository.findOneByEmail(email) != null) {
            validationResult.put("isValid", false);
            validationResult.put("message", "Email already exists in the system");
        } else {
            validationResult.put("isValid", true);
            validationResult.put("message", "Email is available");
        }

        return validationResult;
    }

}
