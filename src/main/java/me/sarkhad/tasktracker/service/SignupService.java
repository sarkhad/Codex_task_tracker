package me.sarkhad.tasktracker.service;

import java.util.Map;

/**
 * Service for creating new users and confirmation
 *
 * @author a.dobrynin
 * @since 0.0.2
 */
public interface SignupService {

    boolean register(Map<Object, Object> user);

    Map<String, Object> isValidEmail(String email);

}
