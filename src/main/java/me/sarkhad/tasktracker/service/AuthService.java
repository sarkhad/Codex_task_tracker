package me.sarkhad.tasktracker.service;

import me.sarkhad.tasktracker.domain.User;

import java.util.Map;

/**
 * @author a.dobrynin
 * @since 0.0.2
 */
public interface AuthService {

    String authenticate(String username, String password);

    User getCurrentUser();

    Map<String, Object> isValidEmail(String email);

}
