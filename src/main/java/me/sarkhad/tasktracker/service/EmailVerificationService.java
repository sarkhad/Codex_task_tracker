package me.sarkhad.tasktracker.service;

/**
 * Service for user email verification
 *
 * @author a.dobrynin
 * @since 0.0.2
 */
public interface EmailVerificationService {

    /**
     * Sends mail with verification token to the given email
     * @param email the user email
     */
    void sendVerificationLink(String email);

    /**
     * Verificate user email
     * @param token the verification token
     */
    void verificate(String token);

}
