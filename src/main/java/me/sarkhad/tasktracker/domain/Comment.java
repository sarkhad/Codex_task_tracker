package me.sarkhad.tasktracker.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    @Getter
    @Setter
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id", nullable = false)
    @Getter
    @Setter
    private Task task;

    @Lob
    @Getter
    @Setter
    @Column
    @JsonProperty
    private String content;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private User author;

}
