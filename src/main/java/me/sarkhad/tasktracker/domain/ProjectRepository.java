package me.sarkhad.tasktracker.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@RepositoryRestResource
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

    Page<Project> queryAllByManagerId(@Param("id") Long id, Pageable pageable);

}
