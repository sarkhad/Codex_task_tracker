package me.sarkhad.tasktracker.domain;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@RepositoryRestResource
@CrossOrigin
public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {
}
