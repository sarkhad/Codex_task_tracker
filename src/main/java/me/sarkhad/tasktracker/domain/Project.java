package me.sarkhad.tasktracker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    @Getter
    @JsonProperty
    private Long id;

    @Column
    @Getter
    @Setter
    @JsonProperty
    private String name;

    @Column
    @Getter
    @Setter
    @JsonProperty
    private String description;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_id", nullable = false)
    private User manager;

    @ManyToMany(mappedBy = "projects", fetch = FetchType.LAZY)
    @JsonIgnore
    @Getter
    @Setter
    private Collection<User> developers;

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    @Getter
    @Setter
    @JsonIgnore
    private Collection<Task> tasks;

}
