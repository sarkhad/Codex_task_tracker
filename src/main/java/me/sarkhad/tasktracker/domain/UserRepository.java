package me.sarkhad.tasktracker.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u " +
            "where lower(u.firstName) " +
            "like lower(CONCAT('%',:name,'%')) or " +
            "lower(u.lastName) like lower(CONCAT('%',:name,'%'))")
    Page<User> searchUsersByPartOfName(@Param("name") String fullName, Pageable pageable);

    User findOneByEmail(String username);

}
