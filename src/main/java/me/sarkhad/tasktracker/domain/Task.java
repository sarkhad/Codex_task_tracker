package me.sarkhad.tasktracker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @JsonProperty
    private Long id;

    @Column(nullable = false)
    @JsonProperty
    @Getter
    @Setter
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false)
    @JsonProperty
    @Getter
    @Setter
    private Project project;

    @Lob
    @Column
    @Getter
    @Setter
    @JsonProperty
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "task")
    @Getter
    @Setter
    @JsonIgnore
    private Collection<Comment> comments;

    @Column
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    @JsonProperty
    private TaskStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    @Getter
    @Setter
    private User reporter;

    @ManyToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    @Getter
    @Setter
    private User assignee;

}
