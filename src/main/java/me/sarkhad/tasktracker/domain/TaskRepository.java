package me.sarkhad.tasktracker.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

    /**
     * Count project's tasks
     *
     * @param id the project id
     * @return how many tasks project has
     */
    Long countByProjectId(@Param("id") Long id);

    Page<Task> findAllByProjectIdAndReporterId(@Param("projectId") Long projectId, @Param("reporterId") Long reporterId, Pageable pageable);

}
