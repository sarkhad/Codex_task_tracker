package me.sarkhad.tasktracker.domain;

/**
 * Types of accounts
 *
 * @author a.dobryinn
 * @since 0.0.1
 */
public enum Role {

    MANAGER, DEVELOPER;

}
