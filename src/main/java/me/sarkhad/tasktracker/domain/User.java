package me.sarkhad.tasktracker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author a.dobrynin
 * @since 0.0.1
 */
@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Getter
    @Column
    private Long id;

    @Column(nullable = false)
    @Getter
    @Setter
    @JsonProperty
    private String firstName;

    @Column(nullable = false)
    @Getter
    @Setter
    @JsonProperty
    private String lastName;

    @Column(nullable = false)
    @Setter
    @JsonIgnore
    private String password;

    @Column(unique = true, nullable = false)
    @Getter
    @Setter
    @JsonProperty
    private String email;

    @ElementCollection(targetClass = Role.class)
    @JsonProperty
    @Getter
    @Setter
    private Collection<Role> roles;

    @OneToMany(mappedBy = "manager", fetch = FetchType.LAZY)
    @JsonIgnore
    @Getter
    @Setter
    private Collection<Project> supervisedProjects;

    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinTable(
            joinColumns = @JoinColumn(referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(referencedColumnName = "id")
    )
    @Getter
    @Setter
    private Collection<Project> projects;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "assignee")
    @Getter
    @Setter
    @JsonIgnore
    private Collection<Task> assignedTasks;

    @Column(nullable = false)
    @Getter
    @Setter
    @JsonIgnore
    private Boolean verified = false;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.toString()));
        }
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    /**
     * By specification there's no support for username. Email is used instead.
     *
     * @return the user email
     */
    @Override
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * By specification user can't log in with no email confirm.
     *
     * @return is verified by email
     */
    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return verified;
    }

}
