package me.sarkhad.tasktracker.domain;

/**
 * @author a.dobrynin
 * @author 0.0.1
 */
public enum TaskStatus {

    WAITING, IMPLEMENTATION, VERIFYING, RELEASING

}
