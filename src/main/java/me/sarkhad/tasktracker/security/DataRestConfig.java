package me.sarkhad.tasktracker.security;

import me.sarkhad.tasktracker.domain.Comment;
import me.sarkhad.tasktracker.domain.Project;
import me.sarkhad.tasktracker.domain.Task;
import me.sarkhad.tasktracker.domain.User;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

/**
 * @author a.dobrynin
 * @since 0.0.2
 */
@Component
public class DataRestConfig extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.getCorsRegistry().addMapping("/**");
        config.exposeIdsFor(Comment.class, Project.class, Task.class, User.class);
    }

}