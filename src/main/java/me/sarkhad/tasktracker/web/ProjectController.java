package me.sarkhad.tasktracker.web;

import me.sarkhad.tasktracker.domain.Project;
import me.sarkhad.tasktracker.domain.ProjectRepository;
import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Map;

/**
 * @author a.dobrynin
 * @since 0.0.2
 * @deprecated Will be reworked with Spring Data
 */
@RestController
@RequestMapping("project")
@Deprecated
public class ProjectController {

    private final ProjectRepository projectRepository;

    private final UserRepository userRepository;

    @Autowired
    public ProjectController(ProjectRepository projectRepository, UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    @PostMapping(value = "add-developer")
    @Transactional
    public void addDeveloper(@RequestBody Map<Object, Object> dto) {
        Long userId = Long.parseLong(dto.get("userId").toString());
        Long projectId = Long.parseLong(dto.get("projectId").toString());

        User user = userRepository.findOne(userId);
        Project project = projectRepository.findOne(projectId);

        Collection<Project> projects = user.getProjects();

        if (!projects.contains(project)) {
            user.getProjects().add(project);
        }

        userRepository.saveAndFlush(user);
    }

}
