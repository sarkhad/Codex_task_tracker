package me.sarkhad.tasktracker.web;

import me.sarkhad.tasktracker.service.EmailVerificationService;
import me.sarkhad.tasktracker.service.SignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * User registration endpoints
 *
 * @author a.dobrynin
 * @since 0.0.2
 */
@RestController
@RequestMapping("/signup")
public class SignupController {

    private final SignupService signupService;

    private final EmailVerificationService emailVerificationService;

    @Autowired
    public SignupController(SignupService signupService, EmailVerificationService emailVerificationService) {
        this.signupService = signupService;
        this.emailVerificationService = emailVerificationService;
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public void confirm(@RequestBody String token) {
        emailVerificationService.verificate(token);
    }

    /**
     * Register user
     *
     * @return the registration result http status
     */
    @PostMapping
    public ResponseEntity createUser(@RequestBody Map<Object, Object> userDto) {
        if (signupService.register(userDto)) {
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.FOUND).body(null);
        }
    }

    @RequestMapping(value = "is-valid-email", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> isValidEmail(@RequestParam String email) {
        return ResponseEntity.ok(signupService.isValidEmail(email));
    }

}
