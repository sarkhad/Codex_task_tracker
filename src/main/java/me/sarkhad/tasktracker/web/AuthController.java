package me.sarkhad.tasktracker.web;

import me.sarkhad.tasktracker.domain.User;
import me.sarkhad.tasktracker.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Authentication endpoints controller
 *
 * @author a.dobrynin
 * @since 0.0.2
 */
@RestController
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    /**
     * Returns the logged user.
     *
     * @return Principal java security principal object
     */
    @GetMapping("/current")
    public User user() {
        return authService.getCurrentUser();
    }

    /**
     * @param username the user email
     * @param password the user password
     * @return authentication token
     */
    @GetMapping("signin")
    public ResponseEntity<Map<String, String>> login(@RequestParam String username, @RequestParam String password) {
        String token = authService.authenticate(username, password);
        Map<String, String> dto = new HashMap<>();
        dto.put("token", token);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @GetMapping(value = "signin/is-valid-email")
    public ResponseEntity<Map<String, Object>> isValidEmail(@RequestParam String email) {
        return ResponseEntity.ok(authService.isValidEmail(email));
    }

}
