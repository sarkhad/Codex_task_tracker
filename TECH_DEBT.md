# TODO
* Token expiration (manually disabled because of annoying expiration and having to re login)
* Rework entity ids fetching with JOOQ - DEPRECATED.
* Implement Dev and Production Profiles.
* Improve api and CORS security. Make it available only for other project micro-services.
* Add integration tests for jpa entities jackson serialization 
* Move ProjectController logic to ProjectRepository PUT method.